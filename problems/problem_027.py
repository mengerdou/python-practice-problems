# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if not values:
        return None
    values[0] = max_num
    for value in values[1:]:
        if value > max_num:
            max_num = value
        return max_num
