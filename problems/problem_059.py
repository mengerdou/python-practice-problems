# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one


import random 


def specific_random():
    list1 = []
    for i in range(100, 501):
        if i % 7 == 0 and i % 5 == 0:
            list1.append(i)
    return random.choice(list1)
    


print(specific_random())