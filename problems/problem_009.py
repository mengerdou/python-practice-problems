# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    # Reverse the word into a list of letters
    # "hello" becomes ["o", "l", "l", "e", "h"]
    # reversed_list_of_letters = reversed(word)

    # Join the letters together using the empty string
    # ["o", "l", "l", "e", "h"] becomes "olleh"
    # reversed_word = "".join(reversed_list_of_letters)

    # If reversed_word equals word
        # return True
    # Otherwise
        # return False
    reverse_word="".join(reversed(word))
    if reverse_word==word:
        return True
    else:
        return False

